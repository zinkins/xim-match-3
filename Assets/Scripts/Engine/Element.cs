﻿using UnityEngine;
using System.Collections;

namespace Xim
{
    public class Element<T> : Element where T : BaseApplication
    {
        /// <summary>
        /// Returns app as a custom 'T' type.
        /// </summary>
        new public T app { get { return (T)base.app; } }
    }

    public class Element : MonoBehaviour
    {

        public BaseApplication app { get { return m_app = Assert<BaseApplication>(m_app, true); } }
        private BaseApplication m_app;

        public T Assert<T>(T p_var, bool p_global = false) where T : Object
        {
            return p_var == null ? (p_global ? GameObject.FindObjectOfType<T>() : transform.GetComponentInChildren<T>()) : p_var;
        }

        public void Notify(string p_event, params object[] p_data) { app.Notify(p_event, this, p_data); }

        public void Notify(float p_delay, string p_event, params object[] p_data) { app.Notify(p_delay, p_event, this, p_data); }

        public void Log(object p_msg, int p_verbose = 0)
        {
            //Only outputs logs equal or bigger than the application 'verbose' level.
            if (p_verbose <= app.verbose) Debug.Log(GetType().Name + "> " + p_msg);
        }
    }
}
