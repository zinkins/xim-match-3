﻿using UnityEngine;
using System.Collections;

namespace Xim
{
    public class Controller : Element
    {
        virtual public void OnNotification(string p_event, Object p_target, params object[] p_data) { }
    }

    public class Controller<T> : Controller where T : BaseApplication
    {
        /// <summary>
        /// Returns app as a custom 'T' type.
        /// </summary>
        new public T app { get { return (T)base.app; } }
    }
}