﻿using UnityEngine;
using System.Collections;

namespace Xim
{
    public class BaseApplication<M, V, C> : BaseApplication
        where M : Element
        where V : Element
        where C : Element
    {
        new public M model { get { return (M)(object)base.model; } }

        new public V view { get { return (V)(object)base.view; } }

        new public C controller { get { return (C)(object)base.controller; } }
    }

    public class BaseApplication : Element
    {
        public int verbose;

        public Model model { get { return m_model = Assert<Model>(m_model); } }
        private Model m_model;

        public View view { get { return m_view = Assert<View>(m_view); } }
        private View m_view;

        public Controller controller { get { return m_controller = Assert<Controller>(m_controller); } }
        private Controller m_controller;

        public void Notify(string p_event, Object p_target, params object[] p_data)
        {
            Controller root = transform.GetComponentInChildren<Controller>();
            Controller[] list = root.GetComponentsInChildren<Controller>();
            Log(p_event + " [" + p_target + "]", 6);
            for (int i = 0; i < list.Length; i++) list[i].OnNotification(p_event, p_target, p_data);
        }
        
    }
}
