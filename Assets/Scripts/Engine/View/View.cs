﻿using UnityEngine;
using System.Collections;

namespace Xim
{
    public class View<T> : View where T : BaseApplication
    {

        /// <summary>
        /// Returns app as a custom 'T' type.
        /// </summary>
        new public T app { get { return (T)base.app; } }

    }

    public class View : Element
    {
        
    }
}
