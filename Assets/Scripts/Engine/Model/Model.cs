﻿using UnityEngine;
using System.Collections;

namespace Xim
{
    public class Model : Element
    {

    }

    public class Model<T> : Model where T : BaseApplication
    {
        /// <summary>
        /// Returns app as a custom 'T' type.
        /// </summary>
        new public T app { get { return (T)base.app; } }
    }
}