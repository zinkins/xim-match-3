﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;

namespace Xim
{
    public class GameElementController : Controller<XimApplication>
    {
        Vector2 direction;
        Vector2 startPosition;

        public override void OnNotification(string p_event, Object p_target, params object[] p_data)
        {
            switch (p_event)
            {
                case (EventConst.GAME_ELEMENT_DRAG_START):
                    PointerEventData data = p_data[0] as PointerEventData;
                    startPosition = Camera.main.ScreenToWorldPoint(data.position);

                    if (Mathf.Abs(data.delta.x) > Mathf.Abs(data.delta.y)) {
                        direction = Vector2.right * Mathf.Sign(data.delta.x);
                    } else
                    {
                        direction = Vector2.down * Mathf.Sign(data.delta.y);
                    }
                    ChangePositionInLayer(p_target as DragView);
                    break;
                case (EventConst.GAME_ELEMENT_DRAG):
                    SetDraggedPosition(p_target as DragView, p_data[0] as PointerEventData);

                    break;
                case (EventConst.GAME_ELEMENT_DRAG_FINISH):
                    direction = Vector2.zero;
                    if (p_data != null && p_data[0] != null)
                        CheckMatch(p_target as DragView, p_data[0] as PointerEventData);
                    break;
                default:
                    break;
            }
        }

        private void ChangePositionInLayer(DragView dragView)
        {
            dragView.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }

        private void SetDraggedPosition(DragView dView, PointerEventData data)
        {
            var rt = dView.target.GetComponent<Transform>();
            Vector3 globalMousePos = Camera.main.ScreenToWorldPoint(data.position);

            // set restrictions in element movement
            rt.position = new Vector3(
                Mathf.Max(app.model.startPosition.x, 
                    Mathf.Min(globalMousePos.x, app.model.startPosition.x + (app.model.width - 1) * app.model.betweenSpace) 
                ),
                Mathf.Min(app.model.startPosition.y,
                    Mathf.Max(globalMousePos.y, app.model.startPosition.y - (app.model.height - 1) * app.model.betweenSpace)
                ), 
                rt.position.z
            );
        }

        private void CheckMatch(DragView dView, PointerEventData data)
        {
            Vector3 globalMousePos = Camera.main.ScreenToWorldPoint(data.position);
            SpatialObject startIndices = app.model.GetGameElementIndices(startPosition.x, startPosition.y);
            SpatialObject endIndices = app.model.GetGameElementIndices(globalMousePos.x, globalMousePos.y);
            
            var gameEl = dView.GetComponent<GameElementView>();

            GameElement firstElement = app.model.GetElement(startIndices);
            GameElement elementToSwap = app.model.GetElement(endIndices);

            SwapElements(startIndices, endIndices);
            var chain1 = app.model.FindChain(firstElement, endIndices);
            var chain2 = app.model.FindChain(elementToSwap, startIndices);
            app.model.RenewBoard();

            if (chain1.Count > 2 || chain2.Count > 2)
            {

                Notify(EventConst.GAME_INITIALIZED);
            }
            else 
            {
                SwapElements(endIndices, startIndices);
            }

            
            dView.GetComponent<SpriteRenderer>().sortingOrder = 0;
            
        }

        private void SwapElements(SpatialObject startIndices, SpatialObject endIndices)
        {
            // swap elements in the Model data
            app.model.SwapElements(startIndices, endIndices);

            // place element on the cell
            app.view.boardView.boardElements[startIndices.X, startIndices.Y].transform.localPosition = 
                app.model.GetWorldCoordinates(endIndices.X, endIndices.Y);
            // place element on the cell
            app.view.boardView.boardElements[endIndices.X, endIndices.Y].transform.localPosition =
                app.model.GetWorldCoordinates(startIndices.X, startIndices.Y);
            // swap elements in the View data
            app.view.boardView.SwapElements(startIndices, endIndices);
            // clear flags and recalculate neighbours after the swappings
            app.model.RenewBoard();
        }
    }
}
