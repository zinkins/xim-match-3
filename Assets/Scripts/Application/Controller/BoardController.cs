﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

namespace Xim
{
    public class BoardController : Controller<XimApplication>
    {
        public override void OnNotification(string p_event, Object p_target, params object[] p_data)
        {
            switch (p_event)
            {
                case (EventConst.GAME_ELEMENT_DRAG_START):
                    
                    break;
                case (EventConst.GAME_ELEMENT_DRAG):
                    RestrictPosition(p_target as DragView, p_data[0] as PointerEventData);
                    break;
                case (EventConst.GAME_ELEMENT_DRAG_FINISH):
                    CheckPosition(p_target as DragView, p_data[0] as PointerEventData);
                    break;
                default:
                    break;
            }
        }


        void CheckPosition(DragView dView, PointerEventData data)
        {

        }

        void RestrictPosition(DragView dView, PointerEventData data)
        {

        }

        public void CheckMatches()
        {

        }
    }
}
