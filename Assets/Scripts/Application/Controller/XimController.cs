﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Xim
{
    public class XimController : Controller<XimApplication>
    {

        public override void OnNotification(string p_event, UnityEngine.Object p_target, params object[] p_data)
        {
            switch (p_event)
            {
                case (EventConst.GAME_INITIALIZED):

                    StartCoroutine(BurstChains(app.model.FindMatches()));
                    
                    break;
            }
        }

        public IEnumerator BurstChains(List<List<GameElement>> list)
        {
            var tmpList = list;
            while (tmpList.Count != 0)
            {
                yield return new WaitForSeconds(app.model.burstDelay);

                foreach (var chain in tmpList)
                {
                    foreach (var element in chain)
                    {
                        app.view.boardView.boardElements[element.X, element.Y].Remove();
                    }
                }
                yield return new WaitForSeconds(app.model.burstDelay);
                GenerateNewElements();

                tmpList = app.model.FindMatches();
            }
        }

        public void GenerateNewElements()
        {
            app.model.Init();
            app.view.Init();
        }
    }
}
