﻿using UnityEngine;
using System.Collections;

namespace Xim
{
    public class BoardView : View<XimApplication>
    {
        public GameElementView[,] boardElements;
        public Transform elementPrefab;
        public Transform spawnParent;
        

        public void SetData(Board board)
        {
            if (boardElements == null)
            {
                boardElements = new GameElementView[app.model.width, app.model.height];

            }

            for (int i = 0; i < app.model.width; i++)
            {
                for (int j = 0; j < app.model.height; j++)
                {
                    Transform t = null;
                    if (boardElements[i, j] == null) {
                        t = Instantiate(elementPrefab);
                    } else
                    {
                        t = boardElements[i, j].transform;
                    }

                    GameElementView elView = t.GetComponent<GameElementView>();
                    elView.SetData(app.model.gameElements[i,j]);
                    elView.Activate();

                    t.name = elementPrefab.name + string.Format(" - {0}-{1}", i, j);
                    t.localPosition = new Vector3( app.model.startPosition.x + i * app.model.betweenSpace,
                        app.model.startPosition.y - j * app.model.betweenSpace, 0 ) ;
                    t.SetParent(spawnParent);
                    boardElements[i, j] = elView;
                }
            }
        }

        public void SwapElements(SpatialObject firstElement, SpatialObject elementToSwap)
        {
            var tmp = boardElements[elementToSwap.X, elementToSwap.Y];
            
            boardElements[elementToSwap.X, elementToSwap.Y] = boardElements[firstElement.X, firstElement.Y];
            
            boardElements[firstElement.X, firstElement.Y] = tmp;
        }
    }
}
