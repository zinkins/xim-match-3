﻿using UnityEngine;
using System.Collections;

namespace Xim
{
    public class XimView : View<XimApplication>
    {
        public BoardView boardView { get { return mBoardView = Assert<BoardView>(mBoardView); } }
        private BoardView mBoardView;

        public void Init()
        {
            if (app.model.board != null)
            {
                boardView.SetData(app.model.board);
            }
        }
        
    }
}
