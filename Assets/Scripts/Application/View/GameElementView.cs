﻿using UnityEngine;
using System.Collections;

namespace Xim
{
    public class GameElementView : View<XimApplication>
    {
        public SpriteRenderer sprite;
        public GameElement element;

        public void SetData(GameElement element)
        {
            this.element = element;
            switch (element.elementType)
            {
                case GameElementType.RED:
                    sprite.color = Color.red;
                    break;
                case GameElementType.BLUE:
                    sprite.color = Color.blue;
                    break;
                case GameElementType.GREEN:
                    sprite.color = Color.green;
                    break;
                case GameElementType.BLACK:
                    sprite.color = Color.black;
                    break;
                case GameElementType.YELLOW:
                    sprite.color = Color.yellow;
                    break;
                default:
                    break;
            }
            
            
        }

        public void Activate()
        {
            gameObject.SetActive(true);
            element.IsRemoved = false;
        }

        public void Remove()
        {
            gameObject.SetActive(false);
            element.IsRemoved = true;
        }
    }
}
