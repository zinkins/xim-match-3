﻿namespace Xim
{
    public enum GameElementType
    {
        RED = 0,
        BLUE = 1,
        GREEN = 2,
        BLACK = 3,
        YELLOW = 4
    }

    public class EventConst
    {
        public const string GAME_ELEMENT_DRAG = "gameElement.drag@drag";
        public const string GAME_ELEMENT_DRAG_START = "gameElement.drag@drag-starts";
        public const string GAME_ELEMENT_DRAG_FINISH = "gameElement.drag@drag-end";
        public const string GAME_INITIALIZED = "game.initialized";
    }
}
