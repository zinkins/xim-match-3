﻿using UnityEngine;
using System.Collections;

namespace Xim
{
    public class XimApplication : BaseApplication<XimModel, XimView, XimController>
    {
        void Start()
        {
            model.Init();
            view.Init();
            print("board element: " + view.boardView.boardElements[0, 0]);
            Notify(EventConst.GAME_INITIALIZED);
        }
    }
}
