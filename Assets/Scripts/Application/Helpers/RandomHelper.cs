﻿using UnityEngine;

namespace Xim
{
    public class RandomHelper
    {

        public static T RandomEnumValue<T>()
        {
            var v = System.Enum.GetValues(typeof(T));
            return (T)v.GetValue(Random.Range(0, v.Length));
        }
    }
}