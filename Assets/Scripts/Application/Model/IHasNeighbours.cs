﻿using System.Collections.Generic;

namespace Xim
{
    public interface IElement<N>
    {
        IEnumerable<N> Neighbours { get; }
        bool IsChecked { get; set; }
        bool IsRemoved { get; set; }
    }
}