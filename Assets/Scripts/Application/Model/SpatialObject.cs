﻿using UnityEngine;
using System.Collections;

namespace Xim
{
    [System.Serializable]
    public struct Point
    {
        public int X, Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }

    [System.Serializable]
    public class SpatialObject 
    {
        public Point location;
        public int X { get { return location.X; } }
        public int Y { get { return location.Y; } }

        public SpatialObject(int x, int y)
            : this(new Point(x, y))
        {
        }

        public SpatialObject(Point location)
        {
            this.location = location;
        }

        public override string ToString()
        {
            return string.Format("[{0}, {1}]", X, Y);
        }

    }
}
