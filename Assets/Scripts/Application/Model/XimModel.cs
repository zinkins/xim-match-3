﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Xim
{
    public class XimModel : Model<XimApplication>
    {
        public Board board;
        public GameElement[,] gameElements;
        public int width, height;
        public float burstDelay;

        public Vector2 startPosition;
        public float betweenSpace;

        public void Init()
        {
            if (board == null)
            {
                board = new Board(width, height);
            }
            if (gameElements == null)
                gameElements = new GameElement[width, height];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (gameElements[i, j] == null)
                    {
                        gameElements[i, j] = new GameElement(i, j, RandomHelper.RandomEnumValue<GameElementType>());
                    } else
                    {
                        if (gameElements[i, j].IsRemoved)
                        {
                            gameElements[i, j].elementType = RandomHelper.RandomEnumValue<GameElementType>();
                            gameElements[i, j].Activate();
                        }
                    }
                    gameElements[i, j].IsChecked = false;
                }
            }

            RecalculateNeighbours();
            
        }

        public void RecalculateNeighbours()
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    gameElements[i, j].FindNeighbours(gameElements);
                }
            }
        }

        public Vector2 GetWorldCoordinates(int i, int j)
        {
            return new Vector2(i * betweenSpace, -j * betweenSpace) + startPosition;
        }

        public SpatialObject GetGameElementIndices(float x, float y)
        {
            Vector2 tmp = new Vector2(x, y) - startPosition;
            int i = (int) Mathf.Max( Mathf.Min((tmp.x / betweenSpace + 0.5f), app.model.width-1), 0);
            int j = (int) Mathf.Max (Mathf.Min((-tmp.y / betweenSpace + 0.5f), app.model.height-1), 0);
            return new SpatialObject(i, j);
        }

        public List<List<GameElement>> FindMatches()
        {
            // we will always compare neighbours, so distance is 1
            Func<GameElement, GameElement, double> distance = (node1, node2) => 1;

            List<List<GameElement>> allChains = new List<List<GameElement>>();

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (!gameElements[i, j].IsChecked)
                    {
                        var chain = ChainFind.FindChain(gameElements[i, j], distance);
                        if (chain.Count >= 3)
                            allChains.Add(chain);
                    }
                }
            }
            Debug.Log("allChains: " + allChains.Count);
            
            return allChains;
        }

        public List<GameElement> FindChain(GameElement gameElement, SpatialObject newPoint)
        {
            // we will always compare neighbours, so distance is 1
            Func<GameElement, GameElement, double> distance = (node1, node2) => 1;

            List<GameElement> chain = ChainFind.FindChain(gameElements[newPoint.X, newPoint.Y], distance);

            if (chain.Count >= 3)
                    Debug.Log("Chains: " + chain.Count);

            return chain;
        }

        public GameElement GetElement(SpatialObject indices)
        {
            return gameElements[indices.X, indices.Y];
        }

        public void SwapElements(SpatialObject firstElement, SpatialObject elementToSwap)
        {
            var tmp = gameElements[elementToSwap.X, elementToSwap.Y];

            gameElements[elementToSwap.X, elementToSwap.Y] = gameElements[firstElement.X, firstElement.Y];
            gameElements[elementToSwap.X, elementToSwap.Y].location = elementToSwap.location;
            
            gameElements[firstElement.X, firstElement.Y] = tmp;
            gameElements[firstElement.X, firstElement.Y].location = firstElement.location;
        }

        public void RenewBoard()
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    gameElements[i, j].IsChecked = false;
                    gameElements[i, j].FindNeighbours(gameElements);
                }
            }
        }
    }
}
