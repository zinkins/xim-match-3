﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Xim
{
    public static class ChainFind
    {
        public static List<Node> FindChain<Node>(
            Node start,
            Func<Node, Node, double> Distance)
            where Node : IElement<Node>
        {
            // this hashSet will contain all non-doubling elements of the chain
            var resultSet = new HashSet<Node>();
            var queue = new PriorityQueue<double, Chain<Node>>();
            // place start element in queue
            queue.Enqueue(0, new Chain<Node>(start));

            // iterate while queue is not empty
            while (!queue.IsEmpty)
            {
                // get chain with highest priority - max distance from start element
                var chain = queue.Dequeue();

                if (resultSet.Contains(chain.LastElement))
                    continue;

                resultSet.Add(chain.LastElement);

                // iterate with neighbours (elements of the same type)
                foreach (Node n in chain.LastElement.Neighbours)
                {
                    double d = Distance(chain.LastElement, n);
                    // create new chain with one additional link
                    var newChain = chain.AddStep(n, d);
                    // place new chain in queue with higher priority
                    queue.Enqueue(newChain.TotalCost + d, newChain);
                }
                
            }

            if (resultSet.Count >= 3)
                Debug.Log("hash count: " + resultSet.Count);

            List<Node> result = new List<Node>();

            foreach (var node in resultSet)
            {
                node.IsChecked = true;
                result.Add(node);
            }

            //int i = 0;
            //foreach (var item in result)
            //{
            //    i++;
            //    Debug.Log("chain #: " + i + " " + item.ToString());
            //}

            return result;
        }
    }
}