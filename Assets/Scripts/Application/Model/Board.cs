﻿using UnityEngine;
using System.Collections;

namespace Xim
{
    [System.Serializable]
    public class Board 
    {
        [SerializeField]
        public Tile[,] boardField;

        public Point dimension;
        public int M { get { return dimension.X; } }
        public int N { get { return dimension.Y; } }


        public Board(int n, int m)
        {
            boardField = new Tile[n,m];
            dimension = new Point(n, m);

            InitialiseGameBoard();
        }

        void InitialiseGameBoard()
        {
            for (int i = 0; i < M; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    boardField[i, j] = new Tile(i, j);
                }
            }
        }
    }
}