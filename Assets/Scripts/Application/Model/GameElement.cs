﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System;

namespace Xim
{
    [System.Serializable]
    public class GameElement : SpatialObject, IElement<GameElement>
    {
        public GameElementType elementType;
        
        public GameElement(Point location) : base(location)
        {
        }

        public GameElement(int x, int y, GameElementType elementType) : base(x, y)
        {
            this.elementType = elementType;
            Activate();
        }

        public IEnumerable<GameElement> AllNeighbours { get; set; }
        public IEnumerable<GameElement> Neighbours { get { return AllNeighbours.Where(o => o.elementType == elementType); } }
        public IEnumerable<GameElement> NeighboursOfType(GameElementType elType)
        {
            return AllNeighbours.Where(o => o.elementType == elType);
        }


        public void FindNeighbours(GameElement[,] gameBoard)
        {
            var neighbours = new List<GameElement>();

            var possibleExits = NeighbourElements;

            foreach (var vector in possibleExits)
            {
                var neighbourX = X + vector.X;
                var neighbourY = Y + vector.Y;

                if (neighbourX >= 0 && neighbourX < gameBoard.GetLength(0) && neighbourY >= 0 && neighbourY < gameBoard.GetLength(1))
                    neighbours.Add(gameBoard[neighbourX, neighbourY]);
            }

            AllNeighbours = neighbours;
        }

        // clear flags
        public void Activate()
        {
            IsRemoved = false;
            IsChecked = false;
        }

        // List of Points containing offsets to neighbour cells
        public static List<Point> NeighbourElements
        {
            get
            {
                return new List<Point>
                {
                    new Point(0, 1),
                    new Point(1, 0),
                    new Point(0, -1),
                    new Point(-1, 0)
                };
            }
        }

        public bool IsChecked { get; set; }
        public bool IsRemoved { get; set; }
    }    
}
