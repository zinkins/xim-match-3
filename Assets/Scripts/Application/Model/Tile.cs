﻿using UnityEngine;
using System.Collections;

namespace Xim
{
    [System.Serializable]
    public class Tile : SpatialObject
    {
        

        public Tile(int x, int y) : base(x, y)
        {
        }
    }
}
