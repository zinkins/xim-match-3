﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Xim
{
    public class Chain<Node> : IEnumerable<Node>
    {
        public Node LastElement { get; private set; }
        public Chain<Node> PreviousElements { get; private set; }
        public double TotalCost { get; private set; }

        private Chain(Node lastElement, Chain<Node> previousElements, double totalCost)
        {
            LastElement = lastElement;
            PreviousElements = previousElements;
            TotalCost = totalCost;
        }

        public Chain(Node start) : this(start, null, 0) { }

        public Chain<Node> AddStep(Node step, double stepCost)
        {
            return new Chain<Node>(step, this, TotalCost + stepCost);
        }

        public IEnumerator<Node> GetEnumerator()
        {
            for (var p = this; p != null; p = p.PreviousElements)
                yield return p.LastElement;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}